// array for holding data of individual chats in singulare objects
const tejas = [{
    Img: "Eknath-Shinde-8.jpg",
    name: "CM Eknath Shinde",
    sentmessage: "saheb khup changale decision ghetay",
    time: "11.20 am",
    chat: "kahi problem asel tr hakkane sang... mi garibanchya adachani dur karnyasathi cm zalo aahe ",
    status: "last seen today at 2.45 pm",
    chatsent: "saheb khup changale decision ghetay"
},

{
    Img: "d0685f5ae5e4e4fe04cc07e9ce815d6c.jpg",
    name: "Ganpati Bappa",
    sentmessage: "hey bappa kasa aahes?",
    time: "2.24 pm",
    chat: "hii",
    status: "online",
    chatsent: "hey bappa kasa aahes?"
},

{
    Img: "tendulkar_mi_ipl_1650777563845_1653936942973.webp",
    name: "Sachin Tendulkar",
    sentmessage: "Hey...God of Cricket",
    time: "5.17 pm",
    chat: "hello",
    status: "last seen today at 11.25 pm",
    chatsent: "Hey...God of Cricket"
},

{
    Img: "download.jfif",
    name: "Narendra Modi",
    sentmessage: "Aapke our kitne year baki he?",
    time: "3.54 pm",
    chat: "mere pyare deshvasio",
    status: "online",
    chatsent: "Aapke our kitne sal baki he?"
},

{
    Img: "170px-Shraddha_Kapoor_graces_the_Star_Screen_Awards_2018_(51)_(cropped).jpg",
    name: "Shraddha Kappor",
    sentmessage: "Hey..beautiful",
    time: "1.35 pm",
    chat: "hey...tejas kasa aahes?",
    status: "last seen today at 3.25 pm",
    chatsent: "Hey..beautiful"
},

{
    Img: "naved.jfif",
    name: "Naved",
    sentmessage: "kuthe aahe rr?",
    time: "8.14 am",
    chat: "hii",
    status: "last seen today at 1.25 pm",
    chatsent: "kuthe aahe rr?"
},

{
    Img: "didi.jfif",
    name: "Didi",
    sentmessage: "kay kartey?",
    time: "10.25 am",
    chat: "kay re kuthe aahes",
    status: "last seen today at 12.16 pm",
    chatsent: "ghari aahe"
},

{
    Img: "siddhesh.jfif",
    name: "Siddhesh Viva",
    sentmessage: "class madhe aaj kay shikavl?",
    time: "9.15 pm",
    chat: "aaj class la ka nahi aalas?",
    status: "online",
    chatsent: "are kam hot...class madhe aaj kay shikavl?"
},

{
    Img: "artist_25336_alia-bhatt-photos-images-68241.jpg",
    name: "Alia Bhatt",
    sentmessage: "konsa month chalu he?",
    time: "3.47 pm",
    chat: "hii",
    status: "last seen today at 9.55 pm",
    chatsent: "konsa month chalu he?"
},

{
    Img: "sanajay.webp",
    name: "Sanajay Raut",
    sentmessage: "Bhai kiticha ghotala kelas",
    time: "12.17 pm",
    chat: "hii",
    status: "last seen toady at 4.37 pm",
    chatsent: "Bhai kiticha ghotala kelas?"
},

{
    Img: "vinayak.jpg",
    name: "Vinayak Dadus",
    sentmessage: "kay sheth..paka aarmuthya hay r tu",
    time: "5.55 pm",
    chat: "kay r bhai",
    status: "online",
    chatsent: "kay sheth..pakka aarmuthya hay r tu"
},

{
    Img: "ambani.jpg",
    name: "Mukesh Ambani",
    sentmessage: "aadhi property mere nam kardo?",
    time: "9.52 am",
    chat: "hii tejas sheth",
    status: "online",
    chatsent: "aadhi property mere nam kardo?"
}
]
// variable text which stores chats html
var text = "";

// for loop thats displays every chat multiple times
for (let i = 0; i < tejas.length; i++) {
    text = text +

        `<div class="sidebar-chat" onclick=myfunction(${i})>
    <div class="chat-avatar">
        <img src=${tejas[i].Img} >
    </div>
    <div class="chat-info">
        <h4>${tejas[i].name}</h4>
        <p>${tejas[i].sentmessage}</p>
    </div>
    <div class="time">
    <p>${tejas[i].time}</p>
    </div>
    </div>`
}
// this statement calls the chats container div and fills the above for loop content as html in the container

document.getElementById('sidebar-chats').innerHTML = text;

// the given function is used to change the background color of object when clicked  
function myfunction(selected) {
    function clickChange(selected) {
        className = "sidebar-chat onclick";
        return className;
    }

    var text = "";
    for (i = 0; i < tejas.length; i++) {
        text = text + `<div class="${selected == i ? clickChange(selected) : "sidebar-chat"}"  onclick=myfunction(${i})>
    <div class="chat-avatar">
        <img src=${tejas[i].Img} alt="">
    </div>
  
    <div class="chat-info">
        <h4>${tejas[i].name}</h4>
        <p>${tejas[i].sentmessage}</p>
    </div>
    <div class="time">
        <p>${tejas[i].time}</p>
    </div>
  </div>`
    }
    document.getElementById('sidebar-chats').innerHTML = text;

    document.getElementById("header").innerHTML = `
  
   
    <div class="chat-title">
        <div class="avatar">
            <img src="${tejas[selected].Img}" alt="">
        </div>
        <div class="message-header-content">
            <h4>${tejas[selected].name}</h4>
            <p>${tejas[selected].status}</p>
        </div>
    </div>
    <div class="chat-header-right">
                <img src="search.svg" alt="">
                <img src="ellipsis-v.svg" alt="">
            </div>`;

    document.getElementById("message-content").innerHTML = `
  
   
            <p class="chat-message">${tejas[selected].chat}<span class="chat-timestamp">${tejas[selected].time}</span></p>
            
            <p class="chat-message chat-sent">${tejas[selected].chatsent}<span class="chat-timestamp">${tejas[selected].time}</span></p>`;




    // responsive using js
    if (window.innerWidth <= "950") {
        document.getElementById("sidebar").style.display = "none";
        document.getElementById("message-container").style.display = "block";
    } else {
        document.getElementById("sidebar").style.display = "block";
        document.getElementById("message-container").style.display = "block";
    }
}




